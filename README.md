# README #

GeneticTSP.java is the main class. 
If you want to test it with a different file, change the filename at line 329 in GeneticTSP.java. 
The way I calculate total work for a circuit is in TSPFitnessWithWeight.java.

How to run it:
DO NOT click new population, that will generate a random set of cities but it has been commented out by me.
Enter all the parameters except "Num Cities" and then click "Go" button.
A recommended Mutation rate will be 0.01, do not use too big of a number. 
When i test it, I usually us Population size of 200-500, Num Generations 10000ish, 
crossover prob 0.2, retained rate 0.1-0.2.

The window will not display any grahs. See the output window for results. 

Also, for genetic Algorithms, you will need to run it on the same set of data several times to get the more optimal solution. 
I normally hit go 3-5 times and pick the best one as the solition. 
